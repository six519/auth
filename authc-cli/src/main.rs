use authc::{AuthClient, AuthToken, Authority, Scheme};
use clap::{load_yaml, App};

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let yml = load_yaml!("cli.yml");
    let app = App::from_yaml(yml);
    let matches = app.clone().get_matches();

    match matches.subcommand() {
        ("register", Some(args)) => {
            let username = get_arg(&args, "username", "Please specify the username.");
            let password = get_arg(&args, "password", "Please specify the password.");
            let auth = set_auth_server();

            if let Err(e) = auth.register(&username, &password).await {
                exit_with(format!("Register failed with: {}", e));
            }
            println!("Successfully registered {}", username);
        }
        ("login", Some(args)) => {
            let username = get_arg(&args, "username", "Please specify the username.");
            let password = get_arg(&args, "password", "Please specify the password.");
            let auth = set_auth_server();

            match auth.sign_in(&username, &password).await {
                Ok(token) => {
                    println!("Auth Token: {}", token.serialize());
                }
                Err(e) => exit_with(format!("Login failed with: {}", e)),
            }
        }
        ("uuid", Some(args)) => {
            let username = get_arg(&args, "username", "Please specify the username.");
            let auth = set_auth_server();

            match auth.username_to_uuid(&username).await {
                Ok(id) => {
                    println!("UUID of {}: {}", username, id);
                }
                Err(e) => exit_with(format!("Retrieving UUID failed with: {}", e)),
            }
        }
        ("validate", Some(args)) => {
            let token: AuthToken =
                match get_arg(&args, "token", "Please specify the token to verify.").parse() {
                    Ok(token) => token,
                    Err(e) => exit_with(format!("failed to parse token: {}", e)),
                };
            let auth = set_auth_server();

            match auth.validate(token).await {
                Ok(id) => {
                    println!("Successfully identified login token for user {}", id);
                }
                Err(e) => exit_with(format!("Validating token failed with: {}", e)),
            }
        }
        (_, _) => {
            exit_with("Need some help buddy?");
        }
    }
}

fn set_auth_server() -> AuthClient {

    let authority: Authority = "127.0.0.1:19253".parse().unwrap();

    let scheme = Scheme::HTTP;

    AuthClient::new(scheme, authority).expect("Insecure URL")
}

fn get_arg<T: std::fmt::Display>(args: &clap::ArgMatches, arg: T, error_msg: T) -> String
where
    T: std::convert::AsRef<str>,
{
    match args.value_of(arg) {
        Some(x) => x.to_string(),
        None => exit_with(error_msg),
    }
}

fn exit_with<T: std::fmt::Display>(message: T) -> ! {
    println!("{}", message);
    std::process::exit(0);
}
